# update 2024-10-22
# import time
import datetime
import matplotlib.pyplot as plt
# import csv
import pandas as pd
# from matplotlib.dates import MonthLocator, DayLocator, HourLocator

plt.close('all')

df=pd.read_csv('measurements.csv',header=0)

#Exctract date time object
def dt2dt_obj(row):
    dt_obj=datetime.datetime.strptime(row['DateTime'], "%Y-%m-%d %H:%M:%S")
    return dt_obj

#Calculate seconds
def dt_obj2sec(row):
    dt_obj=datetime.datetime.strptime(row['DateTime'], "%Y-%m-%d %H:%M:%S")
    secs=dt_obj.timestamp()
    return secs

df['datetime_obj']=df.apply(dt2dt_obj,axis=1)
df['secs']=df.apply(dt_obj2sec,axis=1)
days=[1,3,90]    
dppd=288 # data points per day
fig=plt.figure(figsize=(12, 7))
ax1={}
ax2={}
for i,d in enumerate(days):
    ax1[i]=plt.subplot(len(days),1,i+1)
    dp=dppd*d #days to plot
# fig,ax1=plt.subplots(figsize=(13, 8))
    # plot data:
    temperature=df['inside_temperature'][-dp:]
    humidity=df['inside_humidity'][-dp:]
    ax2[i]=ax1[i].twinx()
    avgp=int((len(temperature))/1000)+1 # mean avg (to dsiplay ~ 1000 pints)
    
    ln1=ax1[i].plot(df['datetime_obj'][-dp:],temperature.rolling(avgp).mean(),
                    'r-',label='temperature')
    ln2=ax2[i].plot(df['datetime_obj'][-dp:],humidity.rolling(avgp).mean(),
                    'b-',label='humidity')
    ax1[i].set_xlabel('time')
    ax1[i].set_ylabel('temperature')
    ax2[i].set_ylabel('humidity')
    # fig.autofmt_xdate()

    ax1[i].grid(True)
    print(f'average points: {avgp}')
#legend
    lns=ln1+ln2
    labs=[l.get_label() for l in lns]
    ax1[i].legend(lns, labs)
    ax1[i].set_title(f'last {d} days')
fig.tight_layout()    
plt.show()
plt.savefig('temperature_log.png',dpi=300)
