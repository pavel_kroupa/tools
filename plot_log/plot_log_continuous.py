# update 2024-10-31
# import time
import datetime
import matplotlib.pyplot as plt
# import csv
import pandas as pd
# import time
# from matplotlib.dates import MonthLocator, DayLocator, HourLocator

plt.close('all')

df=pd.read_csv('measurements.csv',header=0)

#Exctract date time object
def dt2dt_obj(row):
    dt_obj=datetime.datetime.strptime(row['DateTime'], "%Y-%m-%d %H:%M:%S")
    return dt_obj

#Calculate seconds
def dt_obj2sec(row):
    dt_obj=datetime.datetime.strptime(row['DateTime'], "%Y-%m-%d %H:%M:%S")
    secs=dt_obj.timestamp()
    return secs

df['datetime_obj']=df.apply(dt2dt_obj,axis=1)
df['secs']=df.apply(dt_obj2sec,axis=1)

plt.ion() # interactive mode
fig = plt.figure(figsize=(10, 6))


def my_plot(df,d):
    ppd=288 # data points per day
    dp=ppd*d #days to plot
    plt.clf() #clear figure
    ax1=fig.gca()
    ax2=ax1.twinx()

    ln1,=ax1.plot(df['datetime_obj'][-dp:],df['inside_temperature'][-dp:],'r-',label='temperature')
    ln2,=ax2.plot(df['datetime_obj'][-dp:],df['inside_humidity'][-dp:],'b-',label='humidity')

    ax1.set_xlabel('time')
    ax1.set_ylabel('temperature')
    ax2.set_ylabel('humidity')
    fig.autofmt_xdate()
    ax1.grid(True)
    ax1.legend(loc=[0.01,0.9])
    ax2.legend(loc=[0.01,0.8])

for d in range(5):
    my_plot(df,d)
    plt.pause(1)

