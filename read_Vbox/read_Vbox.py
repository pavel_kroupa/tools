import os
import tkinter as tk
from tkinter import filedialog
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# open dialog for folder opening
root=tk.Tk()
root.withdraw()
source_files = filedialog.askopenfilenames(parent=root,title='Choose files to open',
filetypes=[("vbo files", "*.vbo"),('all files', '.*')],multiple=True)


# source_file=source_files[0]
def read_vbo(source_file):
    """ Reads vbo file and creates dataframe with header """
    with open(source_file, 'r') as f:
        content = f.readlines()    # reads whole file
    
    header=content[45].split()    
    dat = np.loadtxt(content[48:], delimiter=' ')  #loading data part to numpy
    
    df=pd.DataFrame()
    df = pd.DataFrame(dat, columns=header)
    
    
    return df

def get_filename_without_extension(file_path):
    file_basename = os.path.basename(file_path)
    # filename_without_extension = file_basename.split('.')[0]
    filename_without_extension = file_basename[:-4]
    return filename_without_extension


def plot_data(data,channels):
    """plots slected data, user can manually offset tiem axis"""
    my_colors=['r','b','g','m','k','c']
    t_offset=[0,-5,0,0,0,0]  # !!!  manual offset for time axis  !!!
    for f, file in enumerate(files):
        for i, channel in enumerate(channels):
            plt.subplot(len(channels),1,i+1)
            plt.plot(data[file]['Exported_Elapsed_time']+t_offset[f],
                     data[file][channel], color=my_colors[f])
            plt.grid('both')
            plt.xlabel('time [s]'), plt.ylabel(channel)
            plt.legend(files)
            

# reading multiple files into dictionary
data={}
for i, filepath in enumerate(source_files):
    name=get_filename_without_extension(filepath) # get file name for key
    data[name]=read_vbo(filepath)

  
files=list(data.keys())    
files.sort() # sorting for plotting
header=data[files[0]].keys()  
print(list(header))

display_channels=['velocity','Y_Accel'] # modify to your channels

plot_data(data,display_channels)

# export figure
folder_for_export= os.path.dirname(os.path.abspath(filepath))
plt.savefig(os.path.join(folder_for_export, 'figure.png'),dpi=300)
