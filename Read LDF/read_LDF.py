"""
Reads csv files from LDF export.
! Make copy of one file and call it Sequence.csv, then write sequence in "Sequence" column
You can add column to Sequence.csv called "Comment", to get additional info in legend
"""
import os
import tkinter as tk
from tkinter import filedialog
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# open dialog for folder opening
root=tk.Tk()
root.withdraw()
source_files = filedialog.askopenfilenames(parent=root,title='Choose files to open',
filetypes=[("csv files", "*.csv"),('all files', '.*')],multiple=True)

def read_data(source_files):        
    """reading all source files and adding df data to dictionary"""
    data={}
    for i, filepath in enumerate(source_files):
        name=get_filename_without_extension(filepath) # get file name for key
        name=remove_request_number(name)
        # print(name)
        data[name]=pd.read_csv(filepath)
    return data

def get_filename_without_extension(file_path):
    file_basename = os.path.basename(file_path)
    filename_without_extension = file_basename[:-4]
    return filename_without_extension
    
def generate_legend_sequence(data):
    # df1=data[list(data.keys())[0]] # 1st excel file (Comment OR data)
    try:
        df1=data['Sequence'] # 1st excel file (Comment OR data)
    except:
        print('no file Sequence.csv found')
        df1=data[list(data.keys())[0]] # 1st data available if no Sequence file exists

    d_len=len(df1) # rows in data
    nconstr=int(d_len/3) # number of constructions (3 recordings per construction)
    legend=[]
    sequence=[]
    for i in range(nconstr):
        construction=df1['Construction number'][i*3]
        try:
            comment=df1['Comment'][i*3]
        except:
            print('no column found in Sequence.csv called: Comment')
            comment=''
        legend.append(construction+' '+str(comment))
        
        try:
            # sequence.append(int(df1.iloc[:,2][i*3]))  
            sequence.append(int(df1['Sequence'][i*3]))  
        except:
            print('no column found in Sequence.csv called: Sequence')
            sequence = [i for i in range(0,nconstr)]

    return legend,sequence

def remove_request_number(text):
    pos=text.find('kPa_')
    if pos>0:
        text=text[:pos+3]+text[pos+14:]
    return text

def addlabels(x,y):
    for i in range(len(x)):
        plt.text(i, y[i], round(y[i],1), ha = 'center',va='bottom')

def add_procent(x, y,miny):
     for i in range(len(x)):
         plt.text(i, miny, str(round(y[i]/y[0]*100,1))+'%', ha = 'center',
                  va='bottom',color='white',weight='bold')
           
def plot_data(data):
    """plots all dat and export figures"""
    my_colors=['r','b','g','m','k','c','y','tab:brown']
    for key in data.keys():
        plot_data=data[key]['avg']
        x_pos=np.arange(len(plot_data))
        plt.figure(key)
        plt.title(key)
        for c in range (len(plot_data)): 
            plt.bar(x_pos[c],plot_data[c],color=my_colors[c])
            
            #plot dots for each measurement:
            for j in range(3):
                plt.plot(c-0.1+(0.1*j),data[key].iloc[c,2+j],'o',alpha=0.5,
                         color='gray')
            
        plt.ylim(bottom=min(plot_data)*0.9)
        plt.xticks(x_pos, data[key]['legend'], rotation='vertical')
        plt.grid(axis='y',alpha=0.3)
        
        addlabels(x_pos, plot_data)
        
        miny=plt.gca().get_ylim()[0]
        add_procent(x_pos, plot_data,miny)
        
        plt.tight_layout()
        
        plt.savefig(os.path.join(os.getcwd(),'fig_'+key+'.png'),dpi=300)

def sort_data(data,legend,sequence):
    data_sorted={}
    for key in data.keys():
        tmp=data[key].iloc[:,1]
        nconstr=int(len(tmp)/3)
        reshaped=tmp.values.reshape(nconstr,3) #  3 columns (one for each measurement)
        d={'legend':legend,'d0':reshaped[:,0],'d1':reshaped[:,1],'d2':reshaped[:,2],
           'avg':reshaped.mean(1),'sequence':sequence}
        df=pd.DataFrame(d)
        dfs=df.sort_values('sequence')
        dfs=dfs.reset_index() # correct sequence fro index
        data_sorted[key]=dfs
    return(data_sorted)    
    

data=read_data(source_files)

legend,sequence=generate_legend_sequence(data)
    
data_sorted=sort_data(data,legend,sequence)

plot_data(data_sorted)