import os
import shutil
import tkinter as tk
from tkinter import filedialog

# open dialog for folder opening
root=tk.Tk()
root.withdraw()
source_folder = filedialog.askdirectory(parent=root,title='Choose files to open')

# List of all files in folder and subfolders:
filelist = []
for root, dirs, files in os.walk(source_folder):
	for file in files:
        #append the file name to the list
		filelist.append(os.path.join(root,file))

#print all the file names
for name in filelist:
    print(name)

# new file path generation
folderlist1=[]
folderlist2=[]  
for name in filelist:
    path,fname=os.path.split(name)
    path_1,dir_level_1=os.path.split(path)
    #unique did level 2 list geberation
    if dir_level_1 not in folderlist1:
        folderlist1.append(dir_level_1)
    path_2,dir_level_2=os.path.split(path_1)
    #unique did level 1 list geberation
    if dir_level_2 not in folderlist2:
        folderlist2.append(dir_level_2)
    new_file=os.path.join(path_2,'new',dir_level_1,dir_level_2,fname)
    print(new_file)
    
    #copy files to new destination:
    new_path,fname=os.path.split(new_file)
    if not os.path.exists(new_path):
        os.makedirs(new_path)
    shutil.copyfile(name,new_file)

print('\nfolderlist1:')
for item in folderlist1: print(item)

print('\nfolderlist2:')
for item in folderlist3: print(item)
