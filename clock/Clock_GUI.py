import tkinter as tk
import time
import requests
clock_app = tk.Tk()
#my_clock.attributes('-fullscreen', True)
#my_clock.geometry("405x170")  
from time import strftime
def time_label():
    time_string = strftime('%H:%M:%S') # time format 
    label1.config(text=time_string)
    #label1.after(1000,time_label) # time delay of 1000 milliseconds 
    date_string = strftime('%A %d. %B') # date format 
    date_label.config(text=date_string)
    date_label.after(1000,time_label) # time delay of 1000 milliseconds 

# get weather
def get_weather():
    BASE_URL="https://api.openweathermap.org/data/2.5/weather?"
    API_KEY="4c793389d28541793aa23ea0f8d592fa"
    LOC="lat=49.8669&lon=6.1825" #Gilsdorf
    #LOC="lat=50.1424&lon=15.1189" #Podebrady
    URL=BASE_URL+LOC+"&units=metric"+"&appid="+API_KEY
    try:
        response=requests.get(URL)
        if response.status_code==200:
            wdata=response.json()#getting data
            temperature=wdata['main']['temp']
            print(f'temperature reading: {temperature}')
        else:
            print("error in HTTP request")
        response.close()
#         print('request closed')
    except:
        wdata={}
        wdata['main']={}
        wdata['main']['temp']=50
        print("unable to get current weather")
    return wdata


def weather_label():
    wdata=get_weather()
    # wdata={}
    # wdata['main']={}
    # wdata['main']['temp']=50
    # temperature=wdata['main']['temp']
    # disp_temp=tk.StringVar()
    # disp_temp.set(wdata['main']['temp'])
    temperature=wdata['main']['temp']
    print(f'temperature display: {temperature}')
    temperature_Label.config(text=temperature)  
    # temperature_Label.config(text=wdata['main']['temp']) 
    temperature_Label.after(60*1000,weather_label) # time delay of 1000 milliseconds 

my_font=('times',52,'bold') # display size and style

Clock_Frame=tk.Frame(clock_app, bg='cyan',padx=3, pady=3)
Bottom_Frame = tk.Frame(clock_app, bg='white',padx=3, pady=3)
Bottom_Frame.grid_columnconfigure(0, weight=1)

Clock_Frame.grid(row=0, column=0, sticky="nsew")
Bottom_Frame.grid(row=1, column=0, sticky ="nsew")	

label1=tk.Label(Clock_Frame,font=my_font,bg='gray')
label1.grid(row=1,column=1,padx=20,pady=50)

date_label=tk.Label(Bottom_Frame, bg='red')
date_label.grid(row=0,column=0)

temperature_Label=tk.Label(Bottom_Frame, bg='green')
temperature_Label.grid(row=2, column=0, columnspan=3)   


time_label()
weather_label()
clock_app.mainloop()
