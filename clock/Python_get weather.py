
import requests
import time

BASE_URL="https://api.openweathermap.org/data/2.5/weather?"

City="Prague"
API_KEY="4c793389d28541793aa23ea0f8d592fa"
pos="lat=50.1424&lon=15.1189"
URL=BASE_URL+pos+"&units=metric"+"&appid="+API_KEY

#HTTP request
response=requests.get(URL)
data=response.json()

temperature=data['main']['temp']

print(f'temperature {temperature}')


# Forecast:
BASE_URL="https://api.openweathermap.org/data/2.5/forecast?"

URL=BASE_URL+pos+"&units=metric"+"&appid="+API_KEY

#HTTP request
response=requests.get(URL)
fdata=response.json()

import json

# as requested in comment
# exDict = {'exDict': exDict}

with open('file.txt', 'w') as file:
     file.write(json.dumps(fdata)) # use `json.loads` to do the reverse
