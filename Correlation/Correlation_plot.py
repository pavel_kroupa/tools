import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from statistics import stdev
# import matplotlib.ticker as mtick # for procent display
import tkinter as tk
from tkinter import filedialog


plt.close('all')




# open dialog for folder opening
root=tk.Tk()
root.withdraw()
source_files = filedialog.askopenfilenames(parent=root,title='Choose files to open',
filetypes=[("xls files", "*.xls*"),('all files', '.*')],multiple=True)

def read_data(source_files):        
    """reading all source files and adding df data to dictionary"""
    data={}
    for i, filepath in enumerate(source_files):
        data=pd.read_excel(filepath, 
                           # sheet_name='Input data',
                           header=2)
    return data

df=read_data(source_files)

# Getting column names and indices:
columns = {i: col for i, col in enumerate(df.columns)}

# Renaming columns:
new_labels = {
        0: 'condition',
        1: 'construction',
        2: 'range',
        3: 'channels',
        4: 'chnumber',
        5: 'level',   
                }

df = df.rename(
    {df.columns[n]: label for n, label in new_labels.items()},
    axis='columns')

df=df[df['condition'].str.contains('0.9')==True] # only accelerated data 0.9

def interpolate(x,y):
    """interpolates X and Y arrays. Returs Ax,B , r_sq coefficients"""
    x=x.reshape(x.shape[0],1)
    regr = linear_model.LinearRegression(fit_intercept=True)
    regr.fit(x, y)
    r_sq = regr.score(x, y)
    A=regr.coef_ #Ax+B
    B=regr.intercept_ #Ax+B
    if abs(r_sq)>100:
        r_sq=0
    return A,B,r_sq


# stdev
def st_dev(df):
    columns = ['construction', 'range', 'channels','stdev']
    dfstd = pd.DataFrame(columns=columns)

    tires=list(df['construction'].unique())
    ranges=list(df['range'].unique())
    channels =list(df['channels'].unique())
    for tire in tires:
        for rms_range in ranges:
            # STD[rms_range]={}
            for channel in channels:
                x=df['level'][df['construction']==tire][df['range']==rms_range]\
                    [df['channels']==channel].values          
                if len(x)>1:
                    STD=stdev(x)
                else: STD=0
                line = {'construction': tire, 'range': rms_range, 'channels': channel, 'stdev':STD}
                dfstd=dfstd.append(line,ignore_index=True)    
    return dfstd
            



def plot_correlation(df, plot_channels, avg_channels):
    """ Plots correlation between mics for various frequency ranges """
    plt.figure(figsize=(18,5))
    charts=list(df['range'].unique())
    # itraration through inique frequency ranges and plotting
    for i, chart in enumerate(charts):
        ref_ch=1
        plt.subplot(1,len(charts),i+1)
        datalegend=[]
    
        for channel in plot_channels:
            if channel !=ref_ch:
                # x=np.array()
                x1=df['level'][df['channels']==avg_channels[0]][df['range']==chart].values
                x2=df['level'][df['channels']==avg_channels[1]][df['range']==chart].values
                x=(x1+x2)/2
                y=df['level'][df['channels']==channel][df['range']==chart].values
    
                # interpolation
                A,B,r_sq=interpolate(x,y)
                regtext='fit: %s %0.2fx %+0.2f    R\N{SUPERSCRIPT TWO}=%0.3f' % (channel,A,B,r_sq)
                # sampletext=f'data samples: {len(x)}'
                datalegend.append(regtext)
               
                plt.plot(x,y,'o',label=regtext)
                # plt.plot(x, A*x+B, color='red', linewidth=1)
                plt.plot(x, A*x+B, color='red', linewidth=0.5)
            plt.xlabel(f'channel {avg_channels}')
            plt.ylabel('other cahnnels')
            plt.title(chart)
            plt.legend()
            plt.grid('both')
            
    
            
    plt.show()
    plt.savefig('Mic correlation.png',dpi=300)

def plot_stdev_table(dfstd):
    """ Plots Standard Deviation table """
    plt.figure(figsize=(18,5))
    charts=list(dfstd['range'].unique())
    for i, chart in enumerate(charts):
        tires=list(df['construction'].unique())
        plt.subplot(1,len(charts),i+1)
        # adding table
        pt2=dfstd[dfstd['channels'].isin(plot_channels)]
        pt=pt2[pt2['range']==chart]
        plot_txt=pt.pivot_table(values="stdev", index=['construction'], columns=['channels'])
        plot_txt=plot_txt.reindex(tires, axis=0) # correct column sequence
        plot_txt=plot_txt.reindex(plot_channels, axis=1) # correct row sequence
        plot_values=np.round(plot_txt.values,2)
        plt.table(cellText=plot_values,rowLabels=tires,colLabels=plot_channels,loc='top', bbox=[0.2,0.7,0.73,.28])
        plt.xticks([]), plt.yticks([])
        plt.title (f'stdev {chart}')
        plt.axis('off')
    plt.show()
    plt.savefig('Standard_Deviation.png',dpi=300)

### plotting

# plot_channels=['Tr_Edge','Ld_Edge','Center_Edge']
plot_channels=['Tr_Edge','Center_Edge','Mic A','Mic B']
# plot_channels=['Tr_Edge']
avg_channels=['Mic A','Mic B']

plot_correlation(df, plot_channels, avg_channels)

dfstd=st_dev(df)
plot_stdev_table(dfstd)
