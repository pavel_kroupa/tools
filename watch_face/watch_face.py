import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from matplotlib.figure import Figure
import os

bgcolor='black'
#seconds marks
smarks={'dout': 100,
        'din':97,
        'width': 2,
        'color': 'white'}
hmarks={'dout': 90,
        'din':74,
        'width': 14,
        'color': 'white'}
bw=4 # border width for hour marks
hm_border={'dout': hmarks['dout'],
        'din': hmarks['din'],
        'width': hmarks['width']+2*bw,
        'color': 'gray'}
    
# drawnumbers=[1,2,4,5,6,9,11,12]
# drawnumbers=[2,4,8,10]
drawnumbers=[6,9]
num_fontsize=80
num_dia=78

logo_position=40 

circ_points=61 # points around the circle

betas=np.linspace(0, 2*np.pi, circ_points)
betah=np.linspace(0, 2*np.pi, 13)

hnum=np.linspace(1,12,12).astype(int) #1-12 numbers
hdict={hour : 0-(hour)/12*2*np.pi+1/2*np.pi for hour in hnum} # angle for numbers

fig, ax = plt.subplots(subplot_kw={'projection': 'polar'},
                       figsize=(8,8), dpi=100,facecolor=bgcolor)

#background:
# ax.fill_between(betas,0, smarks['dout'],color=bgcolor)

for i in betas:    
    ax.plot([i,i],[smarks['din'], smarks['dout']],
            color=smarks['color'],linewidth=smarks['width'])

# 12 hour marks:    
for i in hdict.keys():    
    #thicker second marks:
    ax.plot([hdict[i],hdict[i]],[smarks['din'], smarks['dout']],
            color=smarks['color'],linewidth=smarks['width']+4)
    
    for shape in [hm_border,hmarks]:
        #draw triangle
        if i==12:
            pts=np.array([[hdict[i]-0.08,shape['dout']+0.3], # +0.2 because not on top
                          [hdict[i]+0.08,shape['dout']+0.3],
                          [hdict[i],shape['din']]])
            patch=Polygon(pts, linewidth=shape['width'], color=shape['color']) # create polygin from points
            ax.add_patch(patch)
        else:
            # Hour marks standard:
            ax.plot([hdict[i],hdict[i]],[shape['din'], shape['dout']],
                    color=shape['color'],linewidth=shape['width'])      
            
        
        if i in drawnumbers:   
            # make hour line shorter if number is displayed:
            ax.plot([hdict[i],hdict[i]],[hm_border['din'], hm_border['dout']-5],
                color=bgcolor,linewidth=hm_border['width'])
            # draw numbers:
            ax.text(hdict[i],num_dia,i,va='center', ha='center',size=num_fontsize+2, 
                    fontweight='bold',color='gray')
            
            ax.text(hdict[i],num_dia,i,va='center', ha='center',size=num_fontsize, color='white')
        
#line for battery:
ax.plot([hdict[12]-0.2,hdict[12]+0.2],[82, 82],
            # color=hm_border['color'],
            color='black',
            linewidth=40)   

# logo:
    ##u2655  ,u265B
ax.text(hdict[12],logo_position, "\u265B",
        color='gray',size=70,
        va='bottom', ha='center')
ax.text(hdict[12],logo_position+1, "ROLEX",
        color='gray',size=45,
        va='top', ha='center')

fig.subplots_adjust(left=-0, bottom=-0.0, right=1, top=1,
                    wspace=0, hspace=-0)


ax.grid(b=None)
ax.axis('off')

plt.show()


exp_d='export'
if not os.path.exists(exp_d):
    os.mkdir(exp_d)
plt.savefig('export/wf.png',dpi=300)
